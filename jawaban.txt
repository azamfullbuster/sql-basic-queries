1. BUAT DATABASE

CREATE DATABASE myshop;

2. BUAT TABLE
//table users
CREATE TABLE users(
    user_id int not null auto_increment,
    user_name varchar(255),
    email varchar(255),
    password varchar(255),
    PRIMARY KEY(user_id)
    );

//table categories
CREATE TABLE categories(
    category_id int not null auto_increment,
    category_name varchar(255),
    PRIMARY KEY(category_id)
    );

//table items
CREATE TABLE items(
    item_id int not null auto_increment,
    item_name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    PRIMARY KEY(item_id),
    FOREIGN KEY(category_id) REFERENCES categories(category_id)
    );

3. MEMASUKKAN DATA PADA TABLE
//table users
INSERT INTO users (user_name, email, password) VALUES 
('John Doe', 'john@doe.com', 'john123'), ('Jane Doe', 'jane@doe.com', 'jenita123');

//table categories
INSERT INTO categories (category_name) VALUES ('gadget'), ('cloth'), ('men'), ('women'), ('branded');

//table items
INSERT INTO items(item_name, description, price, stock, category_id) VALUES 
('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
('IMHO Watch', '	jam tangan anak yang jujur banget', 2000000, 10, 1);

4. MENGAMBIL DATA DARI DATABASE

a. mengambil data users
SELECT user_id, user_name, email FROM users;

b. mengambil data items
//harga di atas 1 juta
SELECT * FROM `items` WHERE price>1000000;

//name serupa atau mirip(like) "uniklo", "watch", atau "sang"
SELECT * FROM items WHERE item_name LIKE '%sang%';

c. menampilkan data items join dengan kategori
SELECT items.item_name AS 'Name', items.description, items.price, items.stock, items.category_id, categories.category_name AS 'Kategori' FROM items JOIN categories WHERE items.category_id = categories.category_id;

5. MENGUBAH DATA DARI DATABASE
UPDATE items SET price=2500000 WHERE item_name LIKE '%Sumsang%';